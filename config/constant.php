<?php

return [
    'gender' => [
        0 => 'Not selected',
        1 => 'Male',
        2 => 'Female',
    ],
    'device_type' => [
        1 => 'Check In',
        2 => 'Check Out'
    ],
    'email_type' => [
        'WARNING_LATE' => 'WARNING_LATE'
    ]
];

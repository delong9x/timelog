<div class="panel">
    <div class="panel-heading">
        <h3>Time Log of Employee: {{$employee->name}}</h3>
    </div>
    <div class="panel-body">
        <form>
            <div class="row form-group">
                <label class="col-xs-2" for="year">month:</label>
                <div class="col-xs-3">
                    <select name="month">
                        @for($m = 1; $m <= 12; $m++)
                            <option @if($m == $report_month) selected @endif value="{{$m}}">{{date('F', mktime(0,0,0,$m))}}</option>
                        @endfor
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-xs-2" for="year">Year:</label>
                <div class="col-xs-3">
                    <input class="form-control" name="year" value="{{$report_year}}" />
                </div>
            </div>
            <div class="row form-group">
                <div class="col-xs-12">
                    <input type="submit" class="btn btn-success" value="Filter">
                </div>
            </div>
        </form>
        <hr>
        <h4>Summary in month</h4>
        <ul class="list-unstyled">
            <li class="w-100">
                <label><strong>Total working days:</strong> <span>{{$month_working_days}} days</span></label>
            </li>
            <li class="w-100">
                <label><strong>Total off days:</strong> <span>{{$total_off_days}} days</span></label>
            </li>
            <li class="w-100">
                <label><strong>Total working hours:</strong> <span>{{$total_hours_month_worked}} hours, {{$total_minutes_month_worked}} minutes</span></label>
            </li>
        </ul>
        <hr>
        <h4>Off days in month</h4>
        @if (count($off_days) == 0 )
            <p>No record for off day</p>
        @else
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Time</th>
                    <th>Reason</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($off_days as $day)
                    <tr>
                        <td></td>
                        <td>{{\Carbon\Carbon::parse($day->off_day)->format('d/m/Y')}}</td>
                        <td>{{$day->reason}}</td>
                        <td></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
        <hr>
        <h4>Detail log in month</h4>
        @if (count($list_logs) == 0 )
            <p>No record for log</p>
        @else
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Time</th>
                    <th>Type</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($list_logs as $log)
                    <tr>
                        <td></td>
                        <td>{{\Carbon\Carbon::parse($log->check_at)->format('d/m/Y h:i:s')}}</td>
                        <td>{{$constant_log_type[$log->type]}}</td>
                        <td></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-6">
        <h3>Leaving office today</h3>
        <table class="table table-striped table-dark">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Added At</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($list_leave_today as $item)
                    <tr>
                        <td>{{$item->employee_id}}</td>
                        <td>{{$item->employee->name}}</td>
                        <td>{{$item->employee->email}}</td>
                        <td>{{$item->created_at}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
    <div class="col-xs-12 col-sm-6">
        <h3>Comming office late last week</h3>
        <table class="table table-striped table-dark">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Check In</th>
                <th scope="col">Date</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($list_late_last_week as $k => $item)
                <tr>
                    <td>{{$k + 1}}</td>
                    <td>{{$item->employee->name}}</td>
                    <td>{{$item->employee->email}}</td>
                    <td>{{$item->checkInTime()}}</td>
                    <td>{{Carbon\Carbon::createFromTimeString($item->created_at)->format('d/m/Y')}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
</div>

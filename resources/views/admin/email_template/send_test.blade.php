<div class="panel">
    <div class="panel-heading">
        <h3>Send Test email for: {{$email->template_name}}</h3>
    </div>
    <div class="panel-body">
        <form method="post" action="{{route('emails.doSendTest')}}">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="id" value="{{$email->id}}">
            <div class="form-group">
                <div class="row">
                    <label class="col-xs-4 text-right" for="receiver">Receiver:</label>
                    <div class="col-xs-8">
                        <input class="form-control" type="email" name="receiver" value="">
                    </div>
                </div>
            </div>
            <div class="form-group text-center">
                <div class="row">
                    <div class="col-xs-12">
                        <input type="submit" class="btn btn-success" value="Send">
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>

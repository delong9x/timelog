<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Meta;

class EmailConfigProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        if (\Schema::hasTable('metas')) {
            $drivers = Meta::getByKey('EMAIL_DRIVERS');
            $host = Meta::getByKey('EMAIL_HOST');
            $port = Meta::getByKey('EMAIL_PORT');

            $encryption = Meta::getByKey('EMAIL_ENCRYPTION');
            $username = Meta::getByKey('EMAIL_username');
            $password = Meta::getByKey('EMAIL_password');
            $from_address = Meta::getByKey('EMAIL_FROM_ADDRESS') ? Meta::getByKey('EMAIL_FROM_ADDRESS') : $username ? $username : 'test@mail.email';
            $from_name = Meta::getByKey('EMAIL_FROM_NAME') ? Meta::getByKey('EMAIL_FROM_NAME') : 'test';
            if ($drivers) //checking if table is not empty
            {
                $config = array(
                    'driver'     => $drivers,
                    'host'       => $host,
                    'port'       => $port,
                    'from'       => array('address' => $from_address, 'name' => $from_name),
                    'encryption' => $encryption,
                    'username'   => $username,
                    'password'   => $password,
                    'sendmail'   => '/usr/sbin/sendmail -bs',
                    'pretend'    => false,
                );
                Config::set('mail', $config);
            }
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

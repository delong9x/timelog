<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Mail;

class EmployeeLog extends Model
{
    //
    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }

    public function device(){
        return $this->belongsTo(Device::class, 'device_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class EmailLog extends Model
{
    //
    public function employee() {
        return $this->hasOne(Employee::class, 'id', 'employee_id');
    }

    public function checkInTime() {
        $lateDay = Carbon::createFromTimeString($this->created_at)->format('Y-m-d');
        $checkInLog = EmployeeLog::where('check_at', 'LIKE', $lateDay.'%')->where('employee_id', $this->employee_id)->orderBy('check_at', 'ASC')->first();
        if ($checkInLog) {
            return Carbon::createFromTimeString($checkInLog->check_at)->format('d/m/Y h:i:s');
        }

        return null;

    }
}

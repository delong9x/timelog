<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    //

    function getEmailContent($variable) {
        return $this->replaceVariablesInTemplate($this->content, $variable);
    }

    public function replaceVariablesInTemplate($template, array $variables){

        return preg_replace_callback('#{(.*?)}#',
            function($match) use ($variables){
                $match[1]=trim($match[1],'$');
                return $variables[$match[1]];
            },
            ' '.$template.' ');

    }
}

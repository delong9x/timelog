<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    //

    public static function getByKey($key) {
        $meta = self::where('meta_key', $key)->orderBy('id', 'DESC')->first();
        return $meta ? $meta->meta_data : false;
    }
    public static function setByKey($key, $val) {
        $meta = self::where('meta_key', $key)->orderBy('id', 'DESC')->first();
        if (!$meta) {
            $meta = new Meta();
            $meta->meta_key = $key;
            $meta->meta_data = $val;
            $meta->save();
        } else {
            $meta->meta_data = $val;
            $meta->save();
        }
    }
}

<?php

namespace App\Http\Controllers\Hook;

use App\Models\HookLog;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DeviceController extends Controller
{
    //
    function receive_attendance(Request $request) {
        $data = $request->all();
        $hookLog = new HookLog();
        $webhook = fopen('php://input' , 'rb');
        if (array_key_exists('SN', $data)) {
            $deviceSN = $data['SN'];
            $hookLog->serial_number = $deviceSN;
        }

        $webhookContent = fread($webhook, 4096);
        $hookLog->data = $webhookContent;
        $url = $request->fullUrl();
        $hookLog->url_requested = $url;
        $hookLog->parameter_data = json_encode($data);
        $hookLog->method = $request->getMethod();
        if ($request->getMethod() == 'POST') {
            if (HookLog::where('data', $webhookContent)->count() == 0)
            $hookLog->save();
            return response('OK', 200)->header('Content-Type', 'text/plain');
        }
        if ($data['options'] == 'all') {
            return $this->getAllOptions($data);
        }
    }

    function getAllOptions($data) {
        $text = 'GET OPTION FROM: '.$data['SN'].'
Stamp=9999
OpStamp=9999
ErrorDelay=60
Delay=30
TransTimes=00:00;18:00
TransInterval=1
TransFlag=1111000000
Realtime=1
Encrypt=0
TimeZone=7
ADMSSyncTime=1';
        return response($text, 200)->header('Content-Type', 'text/plain');
    }
}

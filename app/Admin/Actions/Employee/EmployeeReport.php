<?php

namespace App\Admin\Actions\Employee;

use App\Models\EmployeeOffDay;
use Carbon\Carbon;
use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class EmployeeReport extends RowAction
{
    public $name = 'View Report';

    /**
     * @return string
     */
    public function href()
    {
        return "/admin/employee/report/".$this->getKey();
    }

}

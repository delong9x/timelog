<?php

namespace App\Admin\Actions\Employee;

use App\Models\Employee;
use App\Models\EmployeeOffDay;
use Carbon\Carbon;
use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class AddOffDay extends RowAction
{
    protected $selector = '.add-off-day';

    public $name = 'Add Off Day';

    public function handle(Model $model, Request $request)
    {
        // $request ...
        $employeeId = $request->get('employee_id');
        $offDay = $request->get('off_day');
        $reason = $request->get('reason');
        $offDayUTC = Carbon::createFromFormat('d/m/Y', $offDay)->format('Y-m-d');

        $employeeOffDay = new EmployeeOffDay();
        $employeeOffDay->employee_id = $employeeId;
        $employeeOffDay->off_day = $offDayUTC;
        $employeeOffDay->reason = $reason;
        $employeeOffDay->save();
        $employee = Employee::find($employeeId);
        return $this->response()->success('Success add off day for employee: '.$employee->name)->refresh();
    }

    public function form()
    {
        $this->hidden('employee_id')->value($this->getKey());
        $this->date('off_day', 'Date')->format('DD/MM/YYYY')->rules('required');
        $this->textarea('reason', 'Reason');
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: charlie
 * Date: 1/7/20
 * Time: 10:43 AM
 */

namespace App\Admin\Extensions\Tools;

use Encore\Admin\Admin;
use Encore\Admin\Grid\Tools\AbstractTool;
use Illuminate\Support\Facades\Request;

class EmployeeGender
{
    protected function script()
    {
        $url = Request::fullUrlWithQuery(['gender' => '_gender_']);

        return <<<EOT
    
$('input:radio.user-gender').change(function () {

    var url = "$url".replace('_gender_', $(this).val());

    $.pjax({container:'#pjax-container', url: url });

});

EOT;
    }

    public function render()
    {
        Admin::script($this->script());

        $options = [
            0   => 'All',
            1     => 'Male',
            2     => 'Female',
        ];

        return view('admin.tools.gender', compact('options'));
    }
}

<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('admin.home');
    $router->resource('emails', MailController::class);
    $router->get('emails/{id}/send_test', 'MailController@sendTest')->name('emails.sendtest');
    $router->post('emails/send_test', 'MailController@doSendTest')->name('emails.doSendTest');
    $router->resource('employee', EmployeeController::class);
    $router->get('employee/report/{id}', 'EmployeeController@viewReport');
    $router->resource('metas', MetaController::class);
    $router->resource('devices', DeviceController::class);
    $router->resource('employee-logs', EmployeeLogController::class);
    $router->resource('holydays', HolydayController::class);

});

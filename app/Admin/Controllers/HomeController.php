<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\EmailLog;
use App\Models\EmployeeOffDay;
use Carbon\Carbon;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;

class HomeController extends Controller
{
    public function index(Content $content)
    {
        $today = Carbon::today()->format('Y-m-d');

        $previous_week = strtotime("-1 week +1 day");
        $start_week = strtotime("last sunday midnight",$previous_week);
        $end_week = strtotime("next saturday",$start_week);
        $start_week = date("Y-m-d",$start_week);
        $end_week = date("Y-m-d",$end_week);



        $listLeaveToday = EmployeeOffDay::where('off_day', $today)->get();
        $listLateLastWeek = EmailLog::where('code', 'WARNING_LATE')->where('created_at', '<=', $end_week)->where('created_at', '>=', $start_week)->orderBy('created_at', 'DESC')->get();

        return $content
            ->title('Appoint HR System')
            ->view('admin.home.index', ['list_leave_today' => $listLeaveToday, 'list_late_last_week' => $listLateLastWeek])
            ;
    }
}

<?php

namespace App\Admin\Controllers;

use App\Models\Device;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class DeviceController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Device';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Device());

        $grid->column('id', __('Id'));
        $grid->column('serial_number', __('Serial number'));
        $grid->column('type', __('Device Type'))->using(config('constant.device_type'));
        $grid->column('token', __('Token'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Device::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('serial_number', __('Serial number'));
        $show->field('token', __('Token'));
        $show->field('type', __('Device Type'))->using(config('constant.device_type'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Device());

        $form->text('serial_number', __('Serial number'));
        $form->text('token', __('Token'));
        $form->select('type','Device Type')->options(config('constant.device_type'));
        $form->datetime('last_time_sync', __('Last time sync'))->default(date('Y-m-d H:i:s'));

        return $form;
    }
}

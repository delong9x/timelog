<?php

namespace App\Admin\Controllers;

use App\Models\Holyday;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class HolydayController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Holiday';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Holyday());

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();

            // Add a column filter
            $filter->year('from_date', 'Year');

        });

        $grid->column('from_date', __('From date'))->sortable()->date('d/m/Y');
        $grid->column('to_date', __('To date'))->sortable()->date('d/m/Y');
        $grid->column('created_at', __('Created at'))->sortable()->date('d/m/Y');

        $grid->actions(function ($actions) {
            $actions->disableView();
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Holyday::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('from_date', __('From date'));
        $show->field('to_date', __('To date'));
        $show->field('created_at', __('Created at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Holyday());
        $form->dateRange('from_date', 'to_date', __('Days'));
        return $form;
    }
}

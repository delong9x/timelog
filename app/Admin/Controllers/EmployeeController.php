<?php

namespace App\Admin\Controllers;

use App\Admin\Actions\Employee\AddOffDay;
use App\Admin\Actions\Employee\EmployeeReport;
use App\Admin\Extensions\Tools\EmployeeGender;
use App\Models\Employee;
use App\Models\EmployeeLog;
use App\Models\EmployeeOffDay;
use Encore\Admin\Config\Config;
use App\Models\Meta;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\Admin\Extensions\CheckRow;
use Illuminate\Http\Request;
use Carbon\Carbon;

class EmployeeController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Employee';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Employee());
        $grid->quickSearch('name', 'email', 'phone');
        $grid->column('id', __('Id'));
        $grid->column('name', __('Name'));
        $grid->column('email', __('Email'));
        $grid->column('phone', __('Phone'));
        $grid->column('gender', __('Gender'))->using(config('constant.gender'));
        $grid->column('device_user_id', __('Device user id'));
        $grid->column('is_not_late_warning', __('Not warning come office late'))->using([false => 'False', true => 'True']);
        $grid->column('created_at', __('Created at'))->sortable()->date('d/m/Y');

        $grid->actions(function ($actions) {
            $actions->disableView();
            $actions->add(new AddOffDay());
            $actions->add(new EmployeeReport());
            // add action
            $actions->append(new CheckRow($actions->getKey()));

        });


        $grid->filter(function ($filter) {

            // Remove the default id filter
            $filter->disableIdFilter();

            // Add a column filter
            $filter->like('name', 'name');
            $filter->like('email', 'email');

        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Employee::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('email', __('Email'));
        $show->field('phone', __('Phone'));
        $show->field('device_user_id', __('Device user id'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Employee());
        $form->image('avatar')->move('public/upload/avatar/')->uniqueName();
        $form->text('name', __('Name'));
        $form->email('email', __('Email'));
        $form->text('phone', __('Phone'));
        $form->text('device_user_id', __('Device user id'));
        $form->multipleSelect('managers','Managers')->options(function ($id) use ($form) {
            return Employee::where('id', '<>', $form->model()->id)->get()->pluck('name', 'id');
        });
        $form->select('gender', __('Gender'))->options(config('constant.gender'));
        $form->switch('is_not_late_warning', __('Not warning come office late'));
        return $form;
    }


    public function viewReport($id, Request $request)
    {
        $currentMonth = Carbon::now()->month;
        $currentYear = Carbon::now()->year;
        $isCurrent = true;
        if ($request->has('month')) {
            $filterMonth = $request->get('month');
            $filterYear = $request->get('year');
        }
        $reportMonth = $currentMonth;
        $reportYear = $currentYear;
        if (isset($filterMonth)) {
            if (isset($filterYear)) {
                if ($currentYear !=  $filterYear) {
                    $isCurrent = false;
                    $reportMonth = $filterMonth;
                    $reportYear = $filterYear;
                } else {
                    if ($currentMonth != $filterMonth ) {
                        $isCurrent = false;
                        $reportMonth = $filterMonth;
                        $reportYear = $filterYear;
                    }
                }
            }
        }

        if ($isCurrent) {
            $startDateObj = Carbon::now()->startOfMonth();
            $endDateObj = Carbon::now();
        } else {
            $startDateObj = Carbon::createFromFormat('m/Y', $reportMonth.'/'.$reportYear)->startOfMonth();
            $endDateObj = Carbon::createFromFormat('m/Y', $reportMonth.'/'.$reportYear)->endOfMonth();
        }

        $restTimePerDay = Meta::getByKey('REST_TIME_PER_DAY');
        if (!$restTimePerDay) {
            Meta::setByKey('REST_TIME_PER_DAY', 1.5);
            $restTimePerDay = 1.5;
        }
        $totalWorkingMinutes = 0;
        $employee = Employee::find($id);
        $startDate = $startDateObj->format('Y-m-d');
        $endDate = $endDateObj->format('Y-m-d');
        $logQuery = EmployeeLog::whereRaw('MONTH(check_at) >= MONTH(\'' . $startDate . '\') AND MONTH(check_at) <= MONTH(\'' . $endDate . '\') AND YEAR(check_at) = YEAR(\'' . $startDate . '\') AND DAY(check_at) <> DAY(\'' . $endDate . '\') ')
            ->orderBy('check_at', 'ASC')
            ->groupBy('id')
            ->groupBy('check_at');
        $logQuery->where('employee_id', $id);
        $listLogs = $logQuery->get();
        $offDays = EmployeeOffDay::where('employee_id', $id)->whereRaw('MONTH(off_day) >= MONTH(\'' . $startDate . '\') AND MONTH(off_day) <= MONTH(\'' . $endDate . '\') AND YEAR(off_day) = YEAR(\'' . $startDate . '\') AND DAY(off_day) <> DAY(\'' . $endDate . '\') ')->get();
        $listWorkingDays = EmployeeLog::selectRaw('DATE_FORMAT(check_at, \'%d\') as day_month, DATE_FORMAT (check_at, \'%d/%m/%Y\') as check_in')
            ->whereRaw('MONTH(check_at) >= MONTH(\'' . $startDate . '\') AND MONTH(check_at) <= MONTH(\'' . $endDate . '\') AND YEAR(check_at) = YEAR(\'' . $startDate . '\') AND DAY(check_at) <> DAY(\'' . $endDate . '\') ')
            ->where('employee_id', $id)
            ->where('type', 1)
            ->get();

        foreach ($listWorkingDays as $workingDay) {
            $checkIn = EmployeeLog::whereRaw('DATE_FORMAT (check_at, \'%d/%m/%Y\') = \''.$workingDay->check_in.'\'')->where('employee_id', $employee->id)->orderBy('check_at', 'ASC')->where('type', 1)->first();
            $checkOut = EmployeeLog::whereRaw('DATE_FORMAT (check_at, \'%d/%m/%Y\') = \''.$workingDay->check_in.'\'')->where('employee_id', $employee->id)->orderBy('check_at', 'DESC')->where('type', 2)->first();
            if ($checkIn && $checkOut) {
                $checkInTimeObj = Carbon::parse($checkIn->check_at);
                $checkOutTimeObj = Carbon::parse($checkOut->check_at);
                $minutesWorkingInDay = $checkOutTimeObj->diffInMinutes($checkInTimeObj);
                if ($minutesWorkingInDay > 4) {
                    $minutesWorkingInDay -= $restTimePerDay*60;
                }
                $totalWorkingMinutes += $minutesWorkingInDay;
            }
        }
        $totalHoursMonthWorked = (int) $totalWorkingMinutes / 60;
        $totalMinutesMonthWorked = $totalWorkingMinutes - $totalHoursMonthWorked * 60;
        $monthWorkingDays = count($listWorkingDays);
        $data['logs'] = $listLogs;
        $data['employee'] = $employee;
        $data['month_working_days'] = $monthWorkingDays;
        $data['total_off_days'] = count($offDays);
        $data['report_month'] = $reportMonth;
        $data['report_year'] = $reportYear;
        $data['total_working_time'] = $totalWorkingMinutes;
        $data['total_hours_month_worked'] = $totalHoursMonthWorked;
        $data['total_minutes_month_worked'] = $totalMinutesMonthWorked;

        $data['off_days'] = $offDays;
        $data['list_logs'] = $listLogs;
        $data['constant_log_type'] = config('constant.device_type');

        $content = new Content();
        $content->view('admin.employee.report', $data);
        return $content;
    }


}

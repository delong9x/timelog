<?php

namespace App\Admin\Controllers;

use App\Models\EmployeeLog;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class EmployeeLogController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Employee Log';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */

    protected function grid()
    {
        $grid = new Grid(new EmployeeLog());
        $grid->column('id', __('Id'));
        $grid->column('employee.name', __('Employee'))->sortable();
        $grid->column('device.type', __('Type'))->using(config('constant.device_type'))->filter(config('constant.device_type'));
        $grid->column('check_at', __('Check at'))->date('d/m/Y h:i:s')->sortable();
        $grid->filter(function ($filter) {
            $filter->disableIdFilter();
            $filter->column(1/2, function ($filter) {
                $filter->between('check_at','Log time')->datetime();
            });
            $filter->like('employee.name');
        }) ;

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(EmployeeLog::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('employee_id', __('Employee id'));
        $show->field('device_id', __('Device id'));
        $show->field('device_serial_number', __('Device serial number'));
        $show->field('type', __('Type'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('check_at', __('Check at'));
        $show->field('device_user_id', __('Device user id'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new EmployeeLog());

        $form->number('employee_id', __('Employee id'));
        $form->number('device_id', __('Device id'));
        $form->text('device_serial_number', __('Device serial number'));
        $form->switch('type', __('Type'));
        $form->datetime('check_at', __('Check at'))->default(date('Y-m-d H:i:s'));
        $form->text('device_user_id', __('Device user id'));

        return $form;
    }
}

<?php

namespace App\Admin\Controllers;

use App\Models\Email;
use App\Models\Meta;
use Carbon\Carbon;
use Encore\Admin\Actions\RowAction;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Encore\Admin\Layout\Row;
use Request;
use Mail;

class MailController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Email Template';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Email());
        $grid->column('id', __('Id'));
        $grid->column('template_name', __('Template name'));
        $grid->column('code', __('Code'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));
        $grid->actions(function ($action) {
            $action->append('<a href="' . route('emails.sendtest', ['id' => $action->getKey()]) . '">Send Test Email</a>');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Email::findOrFail($id));
        $show->field('id', __('Id'));
        $show->field('template_name', __('Template name'));
        $show->field('code', __('Code'));
        $show->field('content', __('Content'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Email());
        $form->text('template_name', __('Template name'));
        $form->text('code', __('Code'));
        $form->textarea('content', __('Content'));
        $form->tools(function (Form\Tools $tools) {

            // Disable `List` btn.
            $tools->disableList();

            // Disable `View` btn.
            $tools->disableView();

            if(Request::route('email')) {
                $tools->add('<a href="' . route('emails.sendtest', ['id' => Request::route('email')]) . '" class="btn btn-sm btn-success">Test Email</a>');
            }

        });

        return $form;
    }

    protected function sendTest($id)
    {
        $email = Email::find($id);
        if (!$email) {
            admin_error('No Data');
            return redirect(route('emails.index'));
        }
        $data = [];
        $data['email'] = $email;
        $content = new Content();
        $content->view('admin.email_template.send_test', $data);
        return $content;

    }

    protected function doSendTest(\Illuminate\Http\Request $request) {
        $postData = $request->all();
        $id = $postData['id'];
        $email = Email::find($id);
        if (!$email) {
            admin_error('No Data');
            return redirect(route('emails.index'));
        }
        $checkin_time = Carbon::now()->format('d/m/Y h:i:s');
        $name = $postData['receiver'];
        $lateTime = Meta::getByKey('DAILY_SEND_WARNING_LATE_EMAIL') ? Meta::getByKey('DAILY_SEND_WARNING_LATE_EMAIL') : '8:40';
        $emailData = [
            'name' => $name,
            'checkin_time' => $checkin_time,
            'receiver' => $postData['receiver'],
            'subject' => $email->template_name,
            'late_time' => $lateTime
        ];
        $content = $email->getEmailContent($emailData);
        Mail::raw($content, function ($message) use ($emailData) {
            $message->to($emailData['receiver']);
            $message->subject($emailData['subject']);
        });

        return redirect(route('emails.edit', ['email' => $email->id] ));
    }
}

<?php

namespace App\Console\Commands;

use App\Models\EmailLog;
use App\Models\Employee;
use App\Models\EmployeeLog;
use App\Models\EmployeeOffDay;
use App\Models\Email;
use App\Models\Meta;
use App\Models\Holyday;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Mail;

class SendingWarningEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:sentwarning';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check and send email warning comming late';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        $lateTime = Meta::getByKey('DAILY_SEND_WARNING_LATE_EMAIL') ? Meta::getByKey('DAILY_SEND_WARNING_LATE_EMAIL') : '08:40';
        $today = Carbon::now();
        $lateTimeInInt = (int)(str_replace(':', '', trim($lateTime)));
        $isInHoliday = Holyday::where('from_date', '<=' , $today->format('Y-m-d'). ' 00:00:00')->where('to_date', '>=', $today->format('Y-m-d'). ' 00:00:00')->count();
        if (!in_array($today->dayOfWeek, [Carbon::SATURDAY, Carbon::SUNDAY]) && $isInHoliday == 0) {
            $currentHours = $today->format('H:i');
            $listEmployeeTodayOff = EmployeeOffDay::where('off_day','=', $today->format('Y-m-d'))->pluck('employee_id')->toArray();
            if (!is_array($listEmployeeTodayOff)) {
                $listEmployeeTodayOff = [];
            }
            $listCheckedInDayUser = EmployeeLog::select(['type', 'employee_id', 'check_at'])->distinct()->where('check_at', 'LIKE', '%'.$today->format('Y-m-d').'%')->where('type', 1)->pluck('employee_id')->toArray();
            if (!is_array($listCheckedInDayUser)) {
                $listCheckedInDayUser = [];
            }
            $listEmployee = Employee::where('is_not_late_warning', '!=', true)->whereNotNull('device_user_id')->pluck('id')->toArray();
            $listEmployeeWillBeReceived = array_unique(array_diff($listEmployee, array_merge($listEmployeeTodayOff, $listCheckedInDayUser)));
            $listReceivedEmail = EmailLog::where('created_at', 'LIKE', $today->format('Y-m-d').'%')->where('code', config('constant.email_type')['WARNING_LATE'])->pluck('employee_id')->toArray();
            $listEmployeeWillBeReceived = array_diff($listEmployeeWillBeReceived, $listReceivedEmail);
            $currentHourInInt = (int)(str_replace(':', '', trim($currentHours)));
            if ($lateTimeInInt <= $currentHourInInt && count($listEmployeeWillBeReceived) > 0) {
                foreach ($listEmployeeWillBeReceived as $employeeId) {
                    $employee = Employee::find($employeeId);
                    $listCCMail = [];
                    $managerIds = $employee->managers()->select('employee_managers.manager_id')->pluck('manager_id')->toArray();
                    if ($managerIds) {
                        $managers = Employee::whereIn('id', $managerIds)->get();
                        foreach ($managers as $manager) {
                            $listCCMail[] = $manager->email;
                        }
                    }
                    $emailTemplate =  Email::where('code', '=', 'WARNING_LATE_EMAIL')->first();
                    if ($emailTemplate) {
                        $emailData = [
                            'name' => $employee->name,
                            'receiver' => $employee->email,
                            'subject' => $emailTemplate->template_name,
                            'late_time' => $lateTime
                        ];
                        $content = $emailTemplate->getEmailContent($emailData);
                        Mail::raw($content, function ($message) use ($emailData, $listCCMail) {
                            if (env('APP_ENV') == 'production') {
                                $message->to($emailData['receiver']);
                                if ($listCCMail) {
                                    $message->cc($listCCMail);
                                }
                            } else {
                                $message->to(env('MAIL_USERNAME'));
                            }

                            $message->subject($emailData['subject']);

                        });
                        $mailLogs = new EmailLog();
                        $mailLogs->employee_id = $employee->id;
                        $mailLogs->code = config('constant.email_type')['WARNING_LATE'];
                        $mailLogs->email_template_id = $emailTemplate->id;
                        $mailLogs->email_template_code = $emailTemplate->code;
                        $mailLogs->content = $content;
                        $mailLogs->receiver = $employee->email;
                        $mailLogs->save();
                    }
                }
            }
        }
    }
}

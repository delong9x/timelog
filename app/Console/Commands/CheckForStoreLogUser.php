<?php

namespace App\Console\Commands;

use App\Models\Device;
use App\Models\Employee;
use App\Models\EmployeeLog;
use App\Models\HookLog;
use App\Models\Meta;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class CheckForStoreLogUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'logtime:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'For update new records from device';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $isUpdatingConfig = (bool)Meta::getByKey('IS_SYNCHRONIZING');
        if (!$isUpdatingConfig) {
            Meta::setByKey('IS_SYNCHRONIZING', true);
            $lastSync = Meta::getByKey('LAST_SYNC_LOG');
            if ($lastSync) {
                $date = Carbon::parse($lastSync)->format('Y-m-d h:i:s');
            } else {
                $date = '1970-1-1 00:00:00';
                Meta::setByKey('LAST_SYNC_LOG', $date);
            }

            $listSync = HookLog::where('created_at', '>', $date)->where('method','POST')->orderBy('created_at', 'ASC')->get();
            foreach ($listSync as $sync) {
                $urlRequest = $sync->url_requested;
                if (strpos($urlRequest, 'table=ATTLOG') !== false) {
                    $data = preg_split('/[\t]/', $sync->data);
                    $employeeDeviceId = $data[0];
                    $employeeCheckTime = $data[1];
                    $parameterData = json_decode($sync->parameter_data);
                    $deviceSN = $parameterData->SN;
                    $device = Device::where('serial_number', $deviceSN)->first();
                    if (!$device) {
                        $device = new Device();
                        $device->serial_number = $deviceSN;
                        $device->save();
                    }
                    $employee = Employee::where('device_user_id', $employeeDeviceId)->first();
                    $checkExistedRecord = EmployeeLog::where('device_user_id', $employeeDeviceId)->
                    where('device_serial_number', $device->serial_number)->where('check_at', $employeeCheckTime)->count();
                    if ($checkExistedRecord == 0) {
                        $employeeLog = new EmployeeLog();
                        $employeeLog->device_id = $device->id;
                        $employeeLog->device_serial_number = $device->serial_number;
                        $employeeLog->device_user_id = $employeeDeviceId;
                        $employeeLog->type = $device->type;
                        $employeeLog->check_at = $employeeCheckTime;
                        if ($employee) {
                            $employeeLog->employee_id = $employee->id;
                        }
                        $employeeLog->save();
                    }

                    Meta::setByKey('LAST_SYNC_LOG', $sync->created_at);
                }
            }
            Meta::setByKey('IS_SYNCHRONIZING', false);
        }




    }
}

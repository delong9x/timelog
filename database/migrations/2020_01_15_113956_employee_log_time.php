<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EmployeeLogTime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employee_logs', function (Blueprint $table) {
            $table->timestamp('check_at')->nullable();
            $table->string('device_user_id')->nullable();
            $table->string('device_serial_number')->nullable()->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('employee_logs', function (Blueprint $table) {
            $table->dropColumn('check_at');
            $table->dropColumn('device_user_id');
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EmailLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('email_logs', function (Blueprint $table) {
            $table->bigIncrements('id', true);
            $table->string('code')->nullable();
            $table->string('email_template_id')->nullable();
            $table->string('email_template_code')->nullable();
            $table->integer('employee_id')->nullable();
            $table->string('receiver')->nullable();
            $table->longText('content')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('email_logs');
    }
}

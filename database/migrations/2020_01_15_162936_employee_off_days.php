<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EmployeeOffDays extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('employee_off_days', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('off_day')->nullable()->default(null);
            $table->bigInteger('employee_id')->nullable()->default(null);
            $table->string('reason')->nullable()->default(null);
            $table->bigInteger('admin_id')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('employee_off_days');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Holydays extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('holydays', function (Blueprint $table) {
            $table->bigIncrements('id', true);
            $table->timestamp('from_date')->nullable();
            $table->timestamp('to_date')->nullable();
            $table->tinyInteger('is_yearly')->nullable()->default(false);
            $table->timestamps();
        });
        Schema::table('employees', function (Blueprint $table) {
            $table->tinyInteger('is_not_late_warning');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('holydays');
        Schema::table('employees', function (Blueprint $table) {
            $table->dropColumn('is_not_late_warning');
        });
    }
}

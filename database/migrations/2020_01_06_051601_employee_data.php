<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EmployeeData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id', true);
            $table->string('name')->nullable();
            $table->string('email')->unique();
            $table->string('phone')->nullable();
            $table->string('device_user_id')->nullable();
            $table->string('fingerprint_id')->nullable();
            $table->text('avatar')->nullable();
            $table->integer('manager_id')->nullable();
            $table->tinyInteger('gender')->nullable();
            $table->tinyInteger('disabled')->nullable()->default(false);
            $table->timestamp('birthday')->nullable();
            $table->timestamp('start_date')->nullable();
            $table->timestamps();
        });

        Schema::create('devices', function (Blueprint $table) {
            $table->bigIncrements('id', true);
            $table->string('serial_number')->nullable();
            $table->string('token', 250)->nullable();
            $table->timestamp('last_time_sync')->nullable();
            $table->timestamps();
        });

        Schema::create('metas', function (Blueprint $table) {
            $table->bigIncrements('id', true);
            $table->string('meta_key')->nullable();
            $table->text('meta_data')->nullable();
            $table->timestamps();
        });

        Schema::create('employee_logs', function (Blueprint $table) {
            $table->bigIncrements('id', true);
            $table->integer('employee_id')->nullable();
            $table->integer('device_id')->nullable();
            $table->integer('device_serial_number')->nullable();
            $table->tinyInteger('type')->nullable();
            $table->timestamps();
        });

        Schema::create('fingerprints', function (Blueprint $table) {
            $table->bigIncrements('id', true);
            $table->integer('device_id')->nullable();
            $table->text('pin')->nullable();
            $table->text('size')->nullable();
            $table->text('data')->nullable();
            $table->timestamps();
        });

        Schema::create('emails', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('template_name', 250)->nullable();
            $table->string('code', 250)->nullable();
            $table->text('content')->nullable();
            $table->timestamps();
        });




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('employees');
        Schema::dropIfExists('devices');
        Schema::dropIfExists('metas');
        Schema::dropIfExists('employee_logs');
        Schema::dropIfExists('fingerprints');
        Schema::dropIfExists('emails');
    }
}

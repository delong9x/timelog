<?php
use Illuminate\Routing\Router;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect('/admin');
});

Route::group([ 'prefix' => 'hook/device'], function (Router $router) {
    $router->any('/attendance/smartid/cdata', 'Hook\DeviceController@receive_attendance')->name('hook.attendance');
    $router->any('/attendance/smartid/get', 'Hook\DeviceController@receive_attendance')->name('hook.attendance.get');
    $router->any('/attendance/smartid/cmd', 'Hook\DeviceController@receive_attendance')->name('hook.attendance.get');
});
